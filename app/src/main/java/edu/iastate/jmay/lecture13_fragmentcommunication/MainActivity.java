package edu.iastate.jmay.lecture13_fragmentcommunication;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getSupportFragmentManager();
        // Try to find a fragment in the fragmentContainer, if one exists.
        if (fm.findFragmentById(R.id.fragmentContainer) == null) {
            // The Fragment did not exist, so we need to create one.
            Log.d(TAG, "Fragment did not exist.");
            // This is function chaining, where successive calls can be done on return objects,
            // rather than calling each function on a variable.
            // This transaction puts a new MyFragment in the fragmentContainer.
            fm.beginTransaction()
                    .add(R.id.fragmentContainer, new MyFragment())
                    .commit();
        } else {
            // The Fragment exists, so we don't need to create it.
            // This is because the Activity's FragmentManager has automatically stored and reloaded
            // the fragment across a screen rotation.
            Log.d(TAG, "Fragment already exists.");
        }
    }

    public void onMainButtonClick(View v) {
        // This is how an instance of a child Fragment may be located.  Notice the cast to
        // MyFragment.  Casting the object allows calling functions defined in MyFragment (such as
        // setAltMessage()).  Be careful to safeguard your cast if the fragmentContainer may contain
        // classes other than what you are casting to.
        MyFragment childFragment =
                (MyFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        childFragment.setAltMessage();

        // This is how you may modify the layout elements of a child Fragment directly.  Notice that
        // fragmentTextView is NOT defined in activity_main.xml, but rather fragment_my.xml.  This
        // operation will fail if the Fragment isn't loaded or a different Fragment without a
        // fragmentTextView is loaded.
        TextView fragmentTextView = findViewById(R.id.fragmentTextView);
        fragmentTextView.setText(R.string.hello_blank_fragment2);
    }
}
