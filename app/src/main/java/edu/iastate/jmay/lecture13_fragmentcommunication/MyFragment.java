package edu.iastate.jmay.lecture13_fragmentcommunication;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyFragment extends Fragment {
    private boolean altMessage = false;

    public MyFragment() {
        // Required empty public constructor
    }

    public void onFragmentButtonClick(View v) {
        // getActivity() returns the instance of the parent Activity, which is MainActivity in this
        // case, which means findViewById() may locate any View object defined in activity_main.
        TextView mainTextView = getActivity().findViewById(R.id.mainTextView);
        // This if-statement is intended to show that the MainActivity has modified the internal
        // state of its child Fragment.
        if (!altMessage) {
            mainTextView.setText(R.string.textview_main_updated);
        } else {
            mainTextView.setText(R.string.textview_main_updated2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment.  This was created by Android Studio.
        View v = inflater.inflate(R.layout.fragment_my, container, false);

        // This is setting the onClick event handler on the Button in the Fragment's layout.
        Button fragmentButton = v.findViewById(R.id.fragmentButton);
        fragmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFragmentButtonClick(v);
            }
        });

        return v;
    }

    /**
     * Sets the state of this Fragment to display an alternate message when its Button is clicked.
     */
    public void setAltMessage() {
        altMessage = true;
    }
}
